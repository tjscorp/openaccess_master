<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
        <title>OpenAccess Password Reset</title>
        <link rel="icon" type="image/png" href="./img/favicon.ico">
        <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
        <script src="./bootstrap/js/jquery-3.1.1.min.js"></script>
        <script src="./bootstrap/js/bootstrap.min.js"></script>  
        <style type="text/css">

            /* Add whatever you need to your CSS reset */
            html, body, h1, form, fieldset, input {
                margin: 0;
                padding: 0;
                border: none;
            }

            body { font-family: Helvetica, Arial, sans-serif; font-size: 12px; }

            #registration {
                color: #fff;
                background: #2d2d2d;
                background: -webkit-gradient(
                    linear,
                    left bottom,
                    left top,
                    color-stop(0, rgb(60,60,60)),
                    color-stop(0.74, rgb(43,43,43)),
                    color-stop(1, rgb(60,60,60))
                    );
                background: -moz-linear-gradient(
                    center bottom,
                    rgb(60,60,60) 0%,
                    rgb(43,43,43) 74%,
                    rgb(60,60,60) 100%
                    );
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
                border-radius: 10px;
                margin: 10px;
                width: 430px;
            }

            #registration a {
                color: #8c910b;
                text-shadow: 0px -1px 0px #000;
            }

            #registration fieldset {
                padding: 20px;
            }

            input.text {
                -webkit-border-radius: 15px;
                -moz-border-radius: 15px;
                border-radius: 15px;
                border:solid 1px #444;
                font-size: 14px;
                width: 90%;
                padding: 7px 8px 7px 30px;
                -moz-box-shadow: 0px 1px 0px #777;
                -webkit-box-shadow: 0px 1px 0px #777;
                background: #ddd url('img/inputSprite.png') no-repeat 4px 5px;
                background: url('img/inputSprite.png') no-repeat 4px 5px, -moz-linear-gradient(
                    center bottom,
                    rgb(225,225,225) 0%,
                    rgb(215,215,215) 54%,
                    rgb(173,173,173) 100%
                    );
                background:  url('img/inputSprite.png') no-repeat 4px 5px, -webkit-gradient(
                    linear,
                    left bottom,
                    left top,
                    color-stop(0, rgb(225,225,225)),
                    color-stop(0.54, rgb(215,215,215)),
                    color-stop(1, rgb(173,173,173))
                    );
                color:#333;
                text-shadow:0px 1px 0px #FFF;
            }

            input#email {
                background-position: 4px 5px;
                background-position: 4px 5px, 0px 0px;
            }

            input#password {
                background-position: 4px -20px;
                background-position: 4px -20px, 0px 0px;
            }

            input#name {
                background-position: 4px -46px;
                background-position: 4px -46px, 0px 0px;
            }

            input#tel {
                background-position: 4px -76px;
                background-position: 4px -76px, 0px 0px;
            }

            #registration h2 {
                color: #fff;
                text-shadow: 0px -1px 0px #000;
                border-bottom: solid #181818 1px;
                -moz-box-shadow: 0px 1px 0px #3a3a3a;
                text-align: center;
                padding: 18px;
                margin: 0px;
                font-weight: normal;
                font-size: 24px;
                font-family: Lucida Grande, Helvetica, Arial, sans-serif;
            }

            #registerNew {
                width: 203px;
                height: 40px;
                border: none;
                text-indent: -9999px;
                background: url('img/recover.png') no-repeat;
                cursor: pointer;
                float: right;
            }

            #registerNew:hover { background-position: 0px -41px; }
            #registerNew:active { background-position: 0px -82px; }

            #registration p {
                position: relative;
            }

            fieldset label.infield /* .infield label added by JS */ {
                color: #333;
                text-shadow: 0px 1px 0px #fff;
                position: absolute;
                text-align: left;
                top: 3px !important;
                left: 35px !important;
                line-height: 29px;
            }

        </style>
    </head>

    <body>

        <div id="registration">
            <h2>Change WiFi Password</h2>

            <form  action="" method="post" enctype="multipart/form-data" >

                <p>
                    <label for="email">Registered Email e.g. abc@xyz.co.zw</label>
                    <input id="email" name="email" type="email" class="text"  />
                </p>

                <p>
                    <label for="old_pass">Current Password</label>
                    <input id="old_pass" name="old_pass" class="text" type="password" />
                </p>

                <p>
                    <label for="new_pass1">New Password</label>
                    <input id="new_pass1" class="text" type="password" />
                </p>

                <p>
                    <label for="new_pass2">Re-Enter Password</label>
                    <input id="new_pass2" name="new_pass2" class="text" type="password" />
                </p>

                <p>
                <table>
                    <tr>
                    <button style="border:1px solid #F50018; text-align:center; word-wrap: break-word; font-size:12pt; width: 50%; BACKGROUND-COLOR: #8ABDFF; color:#0C20A7"/>Change Password</button>
                    </tr>
                </table>
                </p>


            </form>
            <script type="text/javascript">
                $.ajax({//create an ajax request to getUserInfo.php when doc loads
                    type: "POST",
                    url: "./ajax-scripts/changePassword.php",
                    data: {
                        email: $('#email').val(),
                        old_pass: $('#old_pass').val(),
                        new_pass1: $('#new_pass1').val(),
                        new_pass2: $('#mew_pass2').val(),
                    },
                    dataType: "json", //expect json to be returned  

                    success: function (response) {
                        ress = JSON.stringify(response);
                        res = JSON.parse(ress);
                        status = res['status'];
                        message = res['message'];
                        console.log(ress);

                        switch (status) {
                            case '1':
                                resetCode = res['message'];
                                break;
                            case '0':
                                showNotification('Sorry', message);
                                break;
                            default:
                                showNotification('Sorry', 'An unknown error occured');
                                break;

                        }

                    },
                    error: function (jqXHR, exception) {
                        console.log('AJAX responded with error code: ' + jqXHR.status + ' when it tried to change the password');
                        console.log(jqXHR);
                        showNotification('Sorry', 'There was an error processing this request. Please try again later.');
                    },
                    timeout: 10000
                });
            </script>
        </div>

        <script type="text/javascript">

            $(document).ready(function () {
                (function ($) {
                    $.InFieldLabels = function (label, field, options) {
                        var base = this;
                        base.$label = $(label);
                        base.$field = $(field);
                        base.$label.data("InFieldLabels", base);
                        base.showing = true;
                        base.init = function () {
                            base.options = $.extend({}, $.InFieldLabels.defaultOptions, options);
                            base.$label.css('position', 'absolute');
                            var fieldPosition = base.$field.position();
                            base.$label.css({'left': fieldPosition.left, 'top': fieldPosition.top}).addClass(base.options.labelClass);
                            if (base.$field.val() != "") {
                                base.$label.hide();
                                base.showing = false;
                            }
                            ;
                            base.$field.focus(function () {
                                base.fadeOnFocus();
                            }).blur(function () {
                                base.checkForEmpty(true);
                            }).bind('keydown.infieldlabel', function (e) {
                                base.hideOnChange(e);
                            }).change(function (e) {
                                base.checkForEmpty();
                            }).bind('onPropertyChange', function () {
                                base.checkForEmpty();
                            });
                        };
                        base.fadeOnFocus = function () {
                            if (base.showing) {
                                base.setOpacity(base.options.fadeOpacity);
                            }
                            ;
                        };
                        base.setOpacity = function (opacity) {
                            base.$label.stop().animate({opacity: opacity}, base.options.fadeDuration);
                            base.showing = (opacity > 0.0);
                        };
                        base.checkForEmpty = function (blur) {
                            if (base.$field.val() == "") {
                                base.prepForShow();
                                base.setOpacity(blur ? 1.0 : base.options.fadeOpacity);
                            } else {
                                base.setOpacity(0.0);
                            }
                            ;
                        };
                        base.prepForShow = function (e) {
                            if (!base.showing) {
                                base.$label.css({opacity: 0.0}).show();
                                base.$field.bind('keydown.infieldlabel', function (e) {
                                    base.hideOnChange(e);
                                });
                            }
                            ;
                        };
                        base.hideOnChange = function (e) {
                            if ((e.keyCode == 16) || (e.keyCode == 9))
                                return;
                            if (base.showing) {
                                base.$label.hide();
                                base.showing = false;
                            }
                            ;
                            base.$field.unbind('keydown.infieldlabel');
                        };
                        base.init();
                    };
                    $.InFieldLabels.defaultOptions = {fadeOpacity: 0.5, fadeDuration: 300, labelClass: 'infield'};
                    $.fn.inFieldLabels = function (options) {
                        return this.each(function () {
                            var for_attr = $(this).attr('for');
                            if (!for_attr)
                                return;
                            var $field = $("input#" + for_attr + "[type='text']," + "input#" + for_attr + "[type='password']," + "input#" + for_attr + "[type='tel']," + "input#" + for_attr + "[type='email']," + "textarea#" + for_attr);
                            if ($field.length == 0)
                                return;
                            (new $.InFieldLabels(this, $field[0], options));
                        });
                    };
                })(jQuery);
                $("#RegisterUserForm label").inFieldLabels();
            });
        </script>


        <!--/* Track outbound links in Google Analytics */-->
        <script>
            (function ($) {

                "use strict";
                // current page host
                var baseURI = window.location.host;
                // click event on body
                $("body").on("click", function (e) {

                    // abandon if link already aborted or analytics is not available
                    if (e.isDefaultPrevented() || typeof ga !== "function")
                        return;
                    // abandon if no active link or link within domain
                    var link = $(e.target).closest("a");
                    if (link.length != 1 || baseURI == link[0].host)
                        return;
                    // cancel event and record outbound link
                    e.preventDefault();
                    var href = link[0].href;
                    ga('send', {
                        'hitType': 'event',
                        'eventCategory': 'outbound',
                        'eventAction': 'link',
                        'eventLabel': href,
                        'hitCallback': loadPage
                    });
                    // redirect after one second if recording takes too long
                    setTimeout(loadPage, 1000);
                    // redirect to outbound page
                    function loadPage() {
                        document.location = href;
                    }

                });
            })(jQuery); // pass another library here if required
        </script>
        <br><br>
        Click here to return to the <a href="http://openaccess.co.zw/alogin.html"> LOGIN Page </a>
    </body>

</html>