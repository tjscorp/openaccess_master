<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
        <title>Welcome to OpenAccess WiFi</title>
        <link rel="icon" type="image/png" href="./img/favicon.ico">
        <link href="css/stylish-portfolio.css" rel="stylesheet">
        <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/styles.css">
        <script src="./bootstrap/js/jquery-3.1.1.min.js"></script>
        <script src="./bootstrap/js/bootstrap.min.js"></script> 

    </head>

    <body class="body">

        <?php include './ui_partials/_header.php'; ?>

        <?php include './ui_partials/_sidebar.php'; ?>

        <!-- Header -->
        <div class="main_body">

            <div class="form">
                <p class="custom-welcome-text">Welcome</p>
                <p class="help-text">Keep this page open while you browse</p>

                <iframe src="http://openaccess.co.zw/status" height="250" frameborder="0"></iframe>

                <p class="help-text">You can <a href="http://openaccess.co.zw/logout" >logout of your session!</a><br>
                    Or reset your password <a target="_blank" href="./openaccess_password_reset.php">Here</a>.</p>

            </div>
            <!-- /.container -->

            <?php include './ui_partials/_help_button.php'; ?>

        </div>


        <?php include './ui_partials/_footer.php'; ?>
        
        <?php include './ui_partials/_notifier.php'; ?>

        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-66375359-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
    </body>

</html>
