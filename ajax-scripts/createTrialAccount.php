<?php

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

require '../inc/operator.php';

$operator = new OperatorClass();

$remote_ip = clean_input($_POST['remote_ip']);
$mac = clean_input($_POST['mac']);

$response = $operator->createTrialAccount($remote_ip, $mac);

echo json_encode($response);

