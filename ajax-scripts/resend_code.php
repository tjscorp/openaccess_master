<?php

//error_reporting(-1); // reports all errors
//ini_set("display_errors", "1"); // shows all errors

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

require '../inc/operator.php';

$operator = new OperatorClass();

$mobilenum_email = clean_input($_POST['mobilenum_email']);

$response = $operator->resendAuthCode($mobilenum_email);

echo json_encode($response);

