<?php

//error_reporting(-1); // reports all errors
//ini_set("display_errors", "1"); // shows all errors

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

require '../inc/operator.php';

$operator = new OperatorClass();

$cell = clean_input($_POST['cell']);
$old_pass = clean_input($_POST['old_pass']);
$new_pass = clean_input($_POST['new_pass']);

$response = $operator->changePassword($cell, $old_pass, $new_pass);

echo json_encode($response);

