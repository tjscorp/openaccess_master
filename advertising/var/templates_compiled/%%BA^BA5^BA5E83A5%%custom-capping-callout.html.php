<?php /* Smarty version 2.6.18, created on 2015-04-21 18:17:08
         compiled from /disk1/tawandac/openaccess-www/advertising/lib/templates/admin/form/custom-capping-callout.html */ ?>
<span class="link" help="help-capping-info"><span class="icon icon-info">&nbsp;</span></span>
<div class="hide" id="help-capping-info" style="height: auto; width: 270px;">
    <p>
        Frequency capping depends on the ability to set user cookies.  Selecting this option will allow for serving of ads to users before we know they accept cookies (such as on the first applicable page view), and also allow for unlimited exposure (no frequency capping) to very small number of users that do not accept cookies.
    </p>
</div>





