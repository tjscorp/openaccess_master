<footer class="footer">
    <div id="footer-container">

        <p class="contact-our-team-text">Contact our Team</p>

        <p>Telecontrat Pvt Ltd. Std 475, Pomona, Off Harare Drive, Harare</p>

        <p> 086-8300-0000</p>
        <p><a href="mailto:support@openaccess.co.zw" target="_blank">Email Us Now!</a></p>

        <p class="help-text">Copyright &copy; <a href="http://telco.co.zw" target="_blank">Telco</a> <a href="http://openaccess.co.zw" target="_blank">OpenAccess</a>, <script>document.write(new Date().getFullYear())</script></p>

        </section>
    </div>
</footer>