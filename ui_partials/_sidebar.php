<!-- Navigation -->
<a href="#" id="menu-toggle" class="menu-toggle">
    <span></span>
</a>

<nav id="sidebar-wrapper" class="nav-drawer">

    <a id="menu-close" class="menu-close" href="#" >
        <span></span>
    </a>


    <ul class="sidebar-nav">

        <li class="sidebar-brand">
            <a href="#top"  onclick = $("#menu-close").click(); >OpenAccess WiFi!</a>
        </li>
        <li>
            <a href="#top" onclick = $("#menu-close").click(); >Dashboard</a>
        </li>
        <li>
            <a href="#reset-change-password" onclick = $("#menu-close").click(); >Reset/Change Password</a>
        </li>
        <li>
            <a href="#portfolio" onclick = $("#menu-close").click(); >Account Balance Check</a>
        </li>
        <li>
            <a href="#terms" onclick = $("#menu-close").click(); >Terms & Conditions</a>
        </li>
        <li>
            <a href="#contact" onclick = $("#menu-close").click(); >Contact</a>
        </li>
        <li>
            <a href="http://openaccess.co.zw/logout">Logout</a>
        </li>
    </ul>
</nav>


<script>
    // Closes the sidebar menu
    $("#menu-close").click(function (e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

</script>
