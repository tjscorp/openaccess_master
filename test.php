<?php

//echo sendSms('0771819478', '$message');

//echo sendEmail('tjscorp@gmail.com', '$subjec', '$message');

error_reporting(-1); // reports all errors
ini_set("display_errors", "1"); // shows all errors

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

include './inc/operator.php';

$operator = new OperatorClass();

$auth_code = clean_input($_POST['auth_code']);

$response = $operator->attemptAuthorisation($auth_code);

echo json_encode($response);
